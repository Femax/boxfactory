import javafx.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BoxFactoryStreamTest {

    @Test
    public void onePanel() {
        List data = Arrays.asList(new Pair(2, 2));
        List result = BoxFactoryStream.getBestPanelsByVolume(data, 8);
        List shouldBe = Arrays.asList(new Pair(2, 2), new Pair(2, 2), new Pair(2, 2));
        assert result.equals(shouldBe);
    }

    @Test
    public void onePanelWithBadVolume() {
        List data = Arrays.asList(new Pair(2, 2));
        List result = BoxFactoryStream.getBestPanelsByVolume(data, 12);
        List shouldBe = new ArrayList();
        assert result.equals(shouldBe);
    }

    @Test
    public void somePanels() {
        List data = Arrays.asList(new Pair(3, 3), new Pair(5, 4));
        List result = BoxFactoryStream.getBestPanelsByVolume(data, 8);
        List shouldBe = Arrays.asList(new Pair(3, 3), new Pair(3, 3), new Pair(3, 3));
        assert result.equals(shouldBe);
    }

    // [(2, 4), (2, 3), (1, 3), (2, 2), (3, 5), (1, 2), (3, 4), (1, 4), (2, 5)] and a volume 12, you should return panels [(2, 2), (2, 3), (2, 3)]
    @Test
    public void testKeysFromTask() {
        List data = Arrays.asList(new Pair(2, 4),
                new Pair(2, 3),
                new Pair(1, 3),
                new Pair(2, 2),
                new Pair(3, 5),
                new Pair(1, 2),
                new Pair(3, 4),
                new Pair(1, 4),
                new Pair(2, 5));
        List result = BoxFactoryStream.getBestPanelsByVolume(data, 12);
        List shouldBe = Arrays.asList(new Pair(2, 2), new Pair(2, 3), new Pair(2, 3));
        assert result.equals(shouldBe);
    }
}
