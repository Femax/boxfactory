import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Box factory assembles boxes out of wooden panels.
 * Each panel has a rectangular form.
 * Width and height of a panel are represented by integer numbers.
 * Factory receives an order to  produce a box of a given volume (an integer number again).
 * Chief engineer has a catalog of available panels stored as an array of (width, height) pairs.
 * Your job is to select the cheapest set of three panels that will produce a box of the requested volume.
 * Price of a panel is proportional to it's area.
 * For example, given a panel list [(2, 4), (2, 3), (1, 3), (2, 2), (3, 5), (1, 2), (3, 4), (1, 4), (2, 5)] and a volume 12, you should return panels [(2, 2), (2, 3), (2, 3)]
 */
public class BoxFactoryStream {
    //has Notation O(n^2), coz of 28th row
    public static List<Pair<Integer, Integer>> getBestPanelsByVolume(List<Pair<Integer, Integer>> pairs, Integer volume) {
        return pairs
                .stream()
                .map(it -> new Panel(it.getKey(), it.getValue())) // count Area
                .collect(Collectors.groupingBy(Panel::getWidth)) // group by width
                .entrySet() // java stream crutch
                .stream() // java stream crutch
                .map(it -> new Pair<>(
                                MutationUtils.findMinimalAreaPanel(it.getValue()), // find minimal panel with width
                                MutationUtils.filterByArea(it.getValue(), volume / it.getKey()) // find minimal panel with width
                        )
                )
                .filter(it -> it.getValue() != null)
                .min(Comparator.comparingInt(a -> a.getKey().getWidth()))
                .map(panelsPair ->
                        Arrays.asList(
                                panelsPair.getKey().toPair(),
                                panelsPair.getValue().toPair(),
                                panelsPair.getValue().toPair()
                        )
                ).orElseGet(ArrayList::new);
    }

}
