import javafx.util.Pair;

public class Panel {
    private int height;
    private int width;
    private int area;

    Panel(int width, int height) {
        this.height = height;
        this.width = width;
        this.area = height * width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "Panels{" +
                "height=" + height +
                ", width=" + width +
                ", area=" + area +
                '}';
    }

    public Pair<Integer, Integer> toPair() {
        return new Pair<>(this.width, this.height);
    }
}
